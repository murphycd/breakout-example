# Breakout

Think "block breaker" but super simple. This is an example project for minimal use of the Xygine framework, a "2D Game Engine Framework built around SFML" by fallahn.

* Xygine: https://github.com/fallahn/xygine
* SFML: https://github.com/SFML/SFML


## Before Building

* Install cmake
  * https://cmake.org/download/

#### On Windows:

* Install Visual Studio
  * Tested in 2019. Any version might work. Just make sure to change the generator in do_cmake.bat.


## Building

#### On Windows: 

After installing the prerequisites, run do_cmake.bat. After the build is complete you will be prompted to open the Visual Studio solution located in ```build/breakout.sln```. The project is already built, so it is not necessary to open the IDE. See below for instructions on running the app.

If you do open Visual Studio and with to rebuild without the batch script: in Visual Studio in the Solution Explorer (right side), right-click the project, breakout, and select Set As Startup Project.

Once built, the app can be run from ```build/<configuration>/breakout.exe``` where the configuration is Release or Debug.

#### Linux

Untested, but supported by cmake and the C++ sourcecode.

#### MacOS

Untested, but supported by cmake and the C++ sourcecode.
