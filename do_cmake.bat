@ECHO OFF
setlocal

SET CMAKE_EXE="C:\Program Files\CMake\bin\cmake.exe"
SET GENERATOR="Visual Studio 16 2019"
SET GIT_EXE=git

SET "PROJECT_DIR=%~dp0"
SET "PROJECT_DIR=%PROJECT_DIR:\=/%"
SET "BUILD_DIR=%PROJECT_DIR%/build"

REM SET XYGINE_GIT_PATH=https://github.com/fallahn/xygine.git
SET XYGINE_GIT_PATH=https://github.com/murphycd/xygine.git
SET XYGINE_GIT_TAG=master
SET "XYGINE_SOURCE_PATH=%BUILD_DIR%/xygine/"
SET "XYGINE_INSTALL_PATH=%BUILD_DIR%/xyginext/"

SET STATIC_LIBS=1

IF %STATIC_LIBS%==1 (
    SET BUILD_SHARED=0
) ELSE (
    SET BUILD_SHARED=1
)

:CREATE_TEMP_DIR
IF NOT EXIST "%BUILD_DIR%" MKDIR "%BUILD_DIR%"
cd "%BUILD_DIR%"

:FETCH_XYGINE
ECHO.
ECHO Looking for Xygine repo...
IF NOT EXIST "%XYGINE_SOURCE_PATH%\.git\" (
    ECHO "Cloning..."
    %GIT_EXE% clone %XYGINE_GIT_PATH% --branch %XYGINE_GIT_TAG% --depth 1 --shallow-submodules || goto error
) ELSE (
    cd "%XYGINE_SOURCE_PATH%"
    ECHO Found xygine...getting latest...
    %GIT_EXE% pull | findstr /C:"Already up to date."

    REM remove output products and force rebuild from pulled changes
    REM if findstr did not find a match
    IF %errorlevel%==1 (
        rd /s/q %XYGINE_INSTALL_PATH%
    )
)

:BUILD_XYGINE
ECHO.
ECHO Looking for Xygine output products...
IF NOT EXIST "%XYGINE_INSTALL_PATH%" (
    cd "%XYGINE_SOURCE_PATH%"
    set "XYGINE_BUILD_PATH=%XYGINE_SOURCE_PATH%/build"
    ECHO Generating Xygine build scripts...
    %CMAKE_EXE% -Wno-dev -G %GENERATOR% -DSFML_USE_STATIC_STD_LIBS:BOOL="%STATIC_LIBS%" -DBUILD_SHARED_LIBS:BOOL="%BUILD_SHARED%" -DCMAKE_CONFIGURATION_TYPES:STRING="Debug;Release;" -DCMAKE_INSTALL_PREFIX:PATH="%XYGINE_INSTALL_PATH%" -S "%XYGINE_SOURCE_PATH%" -B "%XYGINE_BUILD_PATH%" || goto error
    ECHO Build Xygine project...
    %CMAKE_EXE% --build "%XYGINE_BUILD_PATH%" --target install --config release || goto error
    %CMAKE_EXE% --build "%XYGINE_BUILD_PATH%" --target install --config debug || goto error
) ELSE (
    ECHO Found %XYGINE_INSTALL_PATH%
)

:GENERATE
ECHO.
ECHO Generating project using CMAKE ...
cd "%PROJECT_DIR%"
ECHO %cd%
%CMAKE_EXE% -G %GENERATOR% -DCMAKE_INSTALL_PREFIX:PATH=%PROJECT_DIR% -DCMAKE_BUILD_TYPE:STRING="Debug" -DSFML_DOC_DIR:PATH="%XYGINE_INSTALL_PATH%/doc" -DXYGINEXT_DIR:PATH="%XYGINE_INSTALL_PATH%/lib/cmake/xyginext" -DSFML_STATIC_LIBRARIES:BOOL="%STATIC_LIBS%" -DBUILD_SHARED_LIBS:BOOL="%BUILD_SHARED%" -DSFML_DIR:PATH="%XYGINE_INSTALL_PATH%/lib/cmake/SFML" -DXY_RUNTIME_FILES="%XYGINE_INSTALL_PATH%/bin" -DCMAKE_CONFIGURATION_TYPES:STRING="Debug;Release;" -S %PROJECT_DIR% -B %BUILD_DIR% || goto error

:BUILD
ECHO.
ECHO Building project using %GENERATOR% ...
%CMAKE_EXE% --build %BUILD_DIR% --target install --config release || goto error
%CMAKE_EXE% --build %BUILD_DIR% --target install --config debug || goto error

:DONE
ECHO.
ECHO Done.
goto end

:ERROR
endlocal
exit /b %errorlevel%

:END
endlocal
:EOF
