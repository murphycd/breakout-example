cmake_minimum_required(VERSION 3.15)

# Rename this variable to change the project name
SET(PROJECT_NAME breakout)

# Set up the project
project(${PROJECT_NAME})

# Some default variables which the user may change
option(BUILD_SHARED_LIBS "Whether to build shared libraries" false)
SET(CMAKE_BUILD_TYPE        Debug CACHE STRING  "Choose the type of build (Debug or Release)")

# Set MVSC runtime library to MultiThreaded[Debug][DLL], based on whether debug config and/or using shared libs.
set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>$<$<BOOL:${BUILD_SHARED_LIBS}>:DLL>")

# We're using c++17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# enable some warnings in debug builds with gcc/clang
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
set(CMAKE_CXX_FLAGS_DEBUG "-g -O0 -Wall -Wextra -Wreorder")
elseif (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
set(CMAKE_CXX_FLAGS_DEBUG "-g -O0 -Wall -Wextra -Wreorder -Wheader-guard")
endif()

# Only works with SFML version 2.5 and above
SET(SFML_MIN_VERSION 2.5)
find_package(SFML ${SFML_MIN_VERSION} REQUIRED graphics window audio system network)

# Find xyginext
find_package(XYGINEXT REQUIRED)

# X11 is required on unices
if(UNIX AND NOT APPLE)
  find_package(X11 REQUIRED)
endif()

if(X11_FOUND)
  include_directories(${X11_INCLUDE_DIRS})
endif()

# Project source files
add_subdirectory(include)
add_subdirectory(src)

# Add XY_DEBUG on Debug builds
if (CMAKE_BUILD_TYPE MATCHES Debug) 
  add_definitions(-DXY_DEBUG)
endif()

if (NOT BUILD_SHARED_LIBS)
    add_definitions(-DSFML_STATIC -DXY_STATIC)
endif()

# Create the actual executable (PROJECT_SRC variable is set inside previous steps)
add_executable(${PROJECT_NAME} ${PROJECT_SRC})

# Linker settings
target_link_libraries(${PROJECT_NAME} xyginext)

# Additional include directories
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_SOURCE_DIR}/include)

if(X11_FOUND)
  target_link_libraries(${PROJECT_NAME}
    ${X11_LIBRARIES})
endif()

set_property(TARGET ${PROJECT_NAME} PROPERTY VS_DEBUGGER_WORKING_DIRECTORY "$<TARGET_FILE_DIR:${PROJECT_NAME}>")

# Install assets folder and everything in it
install(DIRECTORY ${CMAKE_SOURCE_DIR}/assets
    DESTINATION $<TARGET_FILE_DIR:${PROJECT_NAME}>
  )

# Install the contents of the XY_RUNTIME_FILES, excluding the containing folder.
# Runtime files used by xygine (and dependencies) at runtime, e.g. OpenAL32.dll.
install(DIRECTORY ${XY_RUNTIME_FILES}/
    DESTINATION $<TARGET_FILE_DIR:${PROJECT_NAME}>
  )
